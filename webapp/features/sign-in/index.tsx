import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Copyright from "../share/copy-right";
import { Controller, useForm } from "react-hook-form";
import { AnyObject } from "@/common/type";
import { formUtils } from "@/utils/form-utils";
import { AuthTokensToCookies, setTokenToCookies } from "@/common/auth";

const theme = createTheme();

export default function SignIn() {

  const { handleSubmit, control } = useForm();
  const onSubmit = (data: AnyObject) => {
    formUtils.submitForm(data, {
      method: "POST",
      endpoint: "/sign-in",
      onGotSuccess: response => {
        // setTokenToCookies({});
        console.log("response: ", response);
      },
      notify: true,
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">Đăng nhập</Typography>
          
          <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <Controller
              name="email"
              control={control}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <TextField
                  onChange={onChange} 
                  value={value}
                  margin="normal"
                  fullWidth
                  id="email"
                  label="Email"
                  autoComplete="email"
                  autoFocus
                  error={!!error}
                  helperText={error ? "vui lòng nhập email" : null}
                />
              )}
              rules={{ required: true }}
            />
            <Controller {...{
              name: "password",
              control,
              rules: { required: true },
              render: ({ field, fieldState }) => {
                return <TextField
                  onChange={field.onChange}
                  value={field.value}
                  margin="normal"
                  required
                  fullWidth
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  error={!!fieldState.error}
                  helperText={fieldState.error ? "vui lòng nhập mật khẩu" : null}
                />;
              },
            }}/>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Ghi nhớ"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >Đăng nhập</Button>
            <Grid container>
              <Grid item xs>
                <Link href="/forgot-password" variant="body2">Quên mật khẩu?</Link>
              </Grid>
              <Grid item>
                <Link href="/signup" variant="body2">Bạn chưa có tài khoản? Đăng ký</Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}