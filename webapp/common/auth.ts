import Cookies from "js-cookie";

export const cookiesAuthName = {
  ACCESS_TOKEN: "accessToken",
  REFRESH_TOKEN: "refreshToken",
  EXPIRES_IN: "expiresIn",
  EXPIRES_AT: "expiresAt",
};

export interface AuthTokensToCookies {
  accessToken: string; 
  refreshToken: string; 
  expiresIn: string;
  expiresAt: string;
}

export const setTokenToCookies = ({
  accessToken, 
  refreshToken, 
  expiresIn,
  expiresAt,
}: AuthTokensToCookies) => {
  Cookies.set(cookiesAuthName.ACCESS_TOKEN, accessToken);
  Cookies.set(cookiesAuthName.REFRESH_TOKEN, refreshToken);
  Cookies.set(cookiesAuthName.EXPIRES_IN, expiresIn);
  Cookies.set(cookiesAuthName.EXPIRES_AT, expiresAt);
};

export const resetTokenOnCookies = () => {
  Object.values(cookiesAuthName).map(n => Cookies.remove(n));
};