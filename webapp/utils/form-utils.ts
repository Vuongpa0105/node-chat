import { AnyObject } from "@/common/type";
import { enqueueSnackbar } from "notistack";
import { messageUtils } from "./message.utils";
import { request } from "./request";

export interface FormProps {
  endpoint: string;
  method: "GET" | "POST" | "DELETE" | "PUT";
  modifyDataBeforeSubmit?: (formValue: AnyObject) => AnyObject;
  beforeSubmit?: (formValue: AnyObject) => boolean;
  onGotError?: (error: AnyObject) => void;
  onGotSuccess?: (response: AnyObject) => void;
  successMessage?: string;
  errorMessage?: string;
  notify?: boolean;
  params?: AnyObject;
}

export const formUtils = {
  submitForm: (submitData: AnyObject = {}, formProps: FormProps) => {
    const {
      endpoint,
      method,
      modifyDataBeforeSubmit,
      beforeSubmit,
      onGotError,
      onGotSuccess,
      successMessage = messageUtils.successMessage[method as keyof typeof messageUtils.errorMessage],
      errorMessage,
      notify = false,
      params = {},
    } = formProps;

    request.interceptors.response.use(
      function (response) {
        if (notify) {
          enqueueSnackbar(successMessage, { variant: "success" });
        }
        onGotSuccess?.(response);
        return response;
      },
      function (error) {
        if (notify) {
          enqueueSnackbar(
            error?.message || errorMessage || messageUtils.errorMessage[method as keyof typeof messageUtils.errorMessage], 
            { variant: "error" },
          );
        }
        onGotError?.(error);
        return error;
      },
    );
    const dataModifier = modifyDataBeforeSubmit?.(submitData) || submitData;
    beforeSubmit?.(dataModifier);
    request({
      method,
      data: dataModifier,
      url: `${endpoint}?filter=${encodeURIComponent(JSON.stringify(params))}`,
    });
  },
};