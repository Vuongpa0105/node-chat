module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    tsconfigRootDir: __dirname,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:@typescript-eslint/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',

    "quotes": [2, "double"],
    "space-infix-ops": 2,
    "space-before-blocks": 2,
    "object-curly-spacing": [2, "always"],
    "comma-dangle": [2, "always-multiline"],
    "keyword-spacing": 2,
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "never",
      "asyncArrow": "always"
    }],
    "@typescript-eslint/indent": [2, 2],
    "@typescript-eslint/ban-types": "off",
    "@typescript-eslint/no-empty-function": "off",
    "@next/next/no-sync-scripts": "off",
    "@next/next/no-html-link-for-pages": "off",
    "@typescript-eslint/ban-ts-comment": "off",

    "prefer-destructuring": 0,
    "no-case-declarations": 0,
    "no-mixed-operators": 0,
    "no-use-before-define": 0,
    "no-useless-escape": 0,
    "no-nested-ternary": 0,
    "no-await-in-loop": 0,
    "prefer-spread": 0,
    "id-length": 0,
    "consistent-return": 0,
    "valid-jsdoc": 0,
    "no-tabs": 0,
    "no-unused-vars": 0,
    "camelcase": 0,
    "linebreak-style": [
      "error",
      "unix"
    ],
    "semi": [
      "error",
      "always"
    ],
  },
};
