import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import bcrypt from "bcrypt";
import { User } from "src/models/user/user.schema";
import { UserService } from "src/models/user/user.service";
import { RegisterDto } from "./dto/register.dto";
import { omit } from "lodash";

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
  ) {}
  
  async hashPassword(password: string): Promise<string> {
    const passwordHash = await bcrypt.hash(password, 12);
    return passwordHash;
  }

  public async register(registrationData: RegisterDto) {
    const hashedPassword = await this.hashPassword(registrationData.password);
    try {
      const createdUser = await this.userService.create({
        ...registrationData,
        password: hashedPassword,
      });
      createdUser.password = undefined;
      return createdUser;
    } catch (error) {
      if (error?.code === "1100") {
        throw new HttpException("User with that email already exists", HttpStatus.BAD_REQUEST);
      }
      throw new HttpException("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async getAuthenticatedUser(email: string, hashedPassword: string): Promise<Omit<User, "password">> {
    try {
      const user = await this.userService.findByEmail(email);
      if (!user) {
        throw new HttpException(`Could not found user by email ${email}`, HttpStatus.NOT_FOUND);
      }

      const isPasswordMatching = await bcrypt.compare(
        hashedPassword,
        user.password
      );
      if (!isPasswordMatching) {
        throw new HttpException("Wrong credentials provided", HttpStatus.BAD_REQUEST);
      }
      
      return omit(user, "password");
    } catch (error) {
      throw new HttpException("Wrong credentials provided", HttpStatus.BAD_REQUEST);
    }
  }

  public async verifyPassword(plainTextPassword: string, hashedPassword: string) {
    const isMatching = await bcrypt.compare(plainTextPassword, hashedPassword);
    return isMatching;
  }
}