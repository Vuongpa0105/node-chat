import { Body, Controller, Get, HttpCode, Post, Req, UseGuards } from "@nestjs/common";
import { Request } from "express";
import { omit } from "lodash";
import { AuthService } from "./auth.service";
import { RegisterDto } from "./dto/register.dto";
import RequestWithUser from "./dto/request-with-user.dto";
import JwtAuthenticationGuard from "./jwt-auth.guard";
import { LocalAuthenticationGuard } from "./local-auth.guard";

@Controller("authentications")
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post("/signup")
  async register(
  @Body() registrationData: RegisterDto
  ) {
    return this.authService.register(registrationData);
  }

  @HttpCode(200)
  @UseGuards(LocalAuthenticationGuard)
  @Post("/login")
  async logIn(
  @Req() request: RequestWithUser
  ) {
    const user = request.user;
    return omit(user, "password");
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get("/me")
  getMe(
  @Req() request: RequestWithUser
  ) {
    const user = request.user;
    return omit(user, "password");
  }
}