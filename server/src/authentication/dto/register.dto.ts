import { IsStrongPassword } from "class-validator";
import { RequestCreateUserDto } from "src/models/user/user.dto";

export class RegisterDto extends RequestCreateUserDto {
  @IsStrongPassword()
    password: string;
}