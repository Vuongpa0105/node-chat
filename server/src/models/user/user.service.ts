import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { RequestCreateUserDto } from "./user.dto";
import { User, UserDocument } from "./user.schema";

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) 
    private readonly userModel: Model<UserDocument>
  ) {}

  async create(requestCreateUser: RequestCreateUserDto) {
    try {
      const user = await this.userModel.create(requestCreateUser);
      return user;
    } catch (e) {
      if (e?.code === "1100") {
        throw new HttpException("User with that email already exists", HttpStatus.BAD_REQUEST);
      }
      throw new HttpException("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async findByEmail(email: string) {
    if (!email) {
      throw new HttpException("Missing email in data", HttpStatus.BAD_REQUEST);
    }

    const user = await this.userModel.findOne({
      where: { email },
    });
    
    return user;
  }
}