import { IsEmail, IsNotEmpty, IsPhoneNumber } from "class-validator";

export class RequestCreateUserDto {
  @IsEmail()
  email: string;

  @IsPhoneNumber()
  phoneNumber: string;

  @IsNotEmpty()
  firstName: string;
  
  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  password: string;
}